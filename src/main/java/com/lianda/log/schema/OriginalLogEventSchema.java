package com.lianda.log.schema;

import com.lianda.log.model.OriginalLogEvent;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * 原始日志数据的反序列化类
 */
public class OriginalLogEventSchema implements DeserializationSchema<OriginalLogEvent> {
    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public OriginalLogEvent deserialize(byte[] bytes) throws IOException {
        return mapper.readValue(new String(bytes), OriginalLogEvent.class);
    }

    @Override
    public boolean isEndOfStream(OriginalLogEvent originalLogEvent) {
        return false;
    }


    @Override
    public TypeInformation<OriginalLogEvent> getProducedType() {
        return TypeInformation.of(OriginalLogEvent.class);
    }
}