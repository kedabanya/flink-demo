package com.lianda.model;

//用户行为类
public class UserAction {
    private String userId; //用户Id
    private long timeStamp; //时间戳
    private String action; //用户动作
    private String productId; //产品Id
    private int price; //次数

    public UserAction() {
    }

    public UserAction(String userId, long timeStamp, String action, String productId, int price) {
        this.userId = userId;
        this.timeStamp = timeStamp;
        this.action = action;
        this.productId = productId;
        this.price = price;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "UserAction{" +
                "userId='" + userId + '\'' +
                ", timeStamp=" + timeStamp +
                ", action='" + action + '\'' +
                ", productId='" + productId + '\'' +
                ", price=" + price +
                '}';
    }
}
