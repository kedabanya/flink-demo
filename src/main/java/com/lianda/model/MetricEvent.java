package com.lianda.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MetricEvent {

    private String name;

    private Long timestamp;

    private Map<String, Object> fields;

    private Map<String, String> tags;
}
