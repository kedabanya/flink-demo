package com.lianda.alert.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LogEvent {
    //日志类型
    private String type;

    // 日志时间戳
    private Long timestamp;

    //日志级别 (debug/info/warn/error)
    private String level;

    //日志信息
    private String message;

    //日志的tag(appId、dockerId、machine hostIp、machine clusterName、...)
    private Map<String, String> tags = new HashMap<>();
}
