package com.lianda.watermark;

import com.lianda.model.Word;
import com.lianda.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;

import javax.annotation.Nullable;

import static com.lianda.utils.DateUtil.YYYY_MM_DD_HH_MM_SS;

@Slf4j
public class WordPeriodicWatermark implements AssignerWithPeriodicWatermarks<Word> {

    private long currentTimestamp = Long.MIN_VALUE;

    @Nullable
    @Override
    public Watermark getCurrentWatermark() {
        long maxTimeLag = 5000;
        return new Watermark(currentTimestamp == Long.MIN_VALUE ? Long.MIN_VALUE : currentTimestamp - maxTimeLag);
    }

    @Override
    public long extractTimestamp(Word element, long previousElementTimestamp) {
        //提取传过来数据里面的时间戳
        long timestamp = element.getTimestamp();
        currentTimestamp = Math.max(timestamp, currentTimestamp);
        log.info("event timestamp = {}, {}, CurrentWatermark = {}, {}", element.getTimestamp(),
                DateUtil.format(element.getTimestamp(), YYYY_MM_DD_HH_MM_SS),
                getCurrentWatermark().getTimestamp(),
                DateUtil.format(getCurrentWatermark().getTimestamp(), YYYY_MM_DD_HH_MM_SS));
        return element.getTimestamp();
    }
}
