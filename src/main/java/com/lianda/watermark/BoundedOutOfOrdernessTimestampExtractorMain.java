package com.lianda.watermark;

import com.lianda.model.Word;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;

/**
 * 测试Watermark
 */
public class BoundedOutOfOrdernessTimestampExtractorMain {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        //并行度设置为 1
        env.setParallelism(1);

        SingleOutputStreamOperator<Word> data = env.socketTextStream("localhost", 9000)
                .map(new MapFunction<String, Word>() {
            @Override
            public Word map(String value) throws Exception {
                String[] split = value.split(",");
                return new Word(split[0], Integer.valueOf(split[1]), Long.valueOf(split[2]));
            }
        });

        //该类用来发出滞后于数据时间的水印
        data.assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<Word>(Time.seconds(10)) {
            @Override
            public long extractTimestamp(Word element) {
                return element.getTimestamp();
            }
        });

        data.print();
        env.execute("watermark demo");
    }
}
